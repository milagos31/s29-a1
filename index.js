const express = require("express");
const app = express();
const port = 3001;

//Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

let users = [];
//GET
app.get("/home", (req, res) => {
    res.send("This is home");
});

app.get("/users", (req, res) => {
    res.send(users);
});

//POST
app.post("/users", (req, res) => {
    users.push(req.body);
    console.table(users);
    res.send(`Successfully added new user: ${req.body.firstName} ${req.body.lastName}`);
});

app.listen(port, () => console.log(`Activity s29 Server running at port: ${port}`));
